# samsung-debloater

gets rid of (imo) unneeded bloat / double apps made by samsung and some google bloat (google lens, google duo, hangouts,...)

make sure you have a backup of the current state of your phone, or that you are ok with doing factory reset because 
if you delete too many apps those are the ways to recover them, or manually installing them again with adb shell, but that is beyond the scope of this script.

tested on samsung galaxy s7 but should work on any device
how to use:

- open the script (.bat for windows, .sh for linux) and scroll through it, delete or add stuff that you do or do not want to delete.
- enable usb debugging on phone
- install adb tools on your laptop
        fedora: `sudo dnf install android-tools`
        debian(based): `sudo apt install adb`
- check if it works by opening a shell & type adb devices -l, you should see a device.
- if it says no permissions check your phone, you might need to accept rsa key of your pc.
- you can get a list of installed packages with 
        `adb shell pm list packages`
- if you want a list without system packages and only third party packages
        `adb shell pm list packages -3`
- if you want a list with only system packages
        `adb shell pm list packages -s`


more info on adb on xda developers forum:
https://www.xda-developers.com/install-adb-windows-macos-linux/

more info about adb commands:
https://stackpointer.io/mobile/android-adb-list-installed-package-names/416/
