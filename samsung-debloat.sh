#!/bin/bash
#Shell script to remove samsung bloat, can regain these apps by factory reset
#sfinder
adb shell pm uninstall -k --user 0 com.samsung.android.app.galaxyfinder
#skype
adb shell pm uninstall -k --user 0 com.skype.raider
#theme store
adb shell pm uninstall -k --user 0 com.samsung.android.themestore
#not a fan of chrome so removing its customization
adb shell pm uninstall -k --user 0 com.sec.android.app.chromecustomizations
#samsung experience
adb shell pm uninstall -k --user 0 com.samsung.android.app.aodservice
#svoice
adb shell pm uninstall -k --user 0 com.samsung.svoice.sync
adb shell pm uninstall -k --user 0 com.samsung.android.svoice
adb shell pm uninstall -k --user 0 com.samsung.android.voicewakeup
adb shell pm uninstall -k --user 0 com.sec.svoice.lang.de_DE
adb shell pm uninstall -k --user 0 com.sec.svoice.lang.en_GB
adb shell pm uninstall -k --user 0 com.sec.svoice.lang.en_US
adb shell pm uninstall -k --user 0 com.sec.svoice.lang.es_ES
adb shell pm uninstall -k --user 0 com.sec.svoice.lang.fr_FR
adb shell pm uninstall -k --user 0 com.sec.svoice.lang.it_IT
adb shell pm uninstall -k --user 0 com.sec.svoice.lang.ru_RU
adb shell pm uninstall -k --user 0 com.samsung.voiceserviceplatform
#google quick search box
adb shell pm uninstall -k --user 0 com.google.android.googlequicksearchbox
#samsung calendar
adb shell pm uninstall -k --user 0 com.samsung.android.calendar
adb shell pm uninstall -k --user 0 com.android.providers.calendar
#smart mirrorring
adb shell pm uninstall -k --user 0 com.samsung.android.smartmirroring
adb shell pm uninstall -k --user 0 com.samsung.android.app.withtv
#samsungs telemetry
adb shell pm uninstall -k --user 0 com.samsung.android.rubin.app
#voice bs
adb shell pm uninstall -k --user 0 com.sec.android.app.voicenote
#samsung email
adb shell pm uninstall -k --user 0 com.samsung.android.email.provider
#some face thing
adb shell pm uninstall -k --user 0 com.samsung.faceservice
#samsung apps / galaxy apps
adb shell pm uninstall -k --user 0 com.sec.android.app.samsungapps
#face AR stuff
adb shell pm uninstall -k --user 0 com.samsung.android.app.camera.sticker.facear.preload
#gamelauncher
adb shell pm uninstall -k --user 0 com.samsung.android.game.gamehome
#google play AR services 
adb shell pm uninstall -k --user 0 com.google.ar.core
#news app upday
adb shell pm uninstall -k --user 0 de.axelspringer.yana.zeropage
#watch manager
adb shell pm uninstall -k --user 0 com.samsung.android.app.watchmanager
#live wallpapers
adb shell pm uninstall -k --user 0 com.samsung.android.smartface
#google music
adb shell pm uninstall -k --user 0 com.google.android.music
#talkback
adb shell pm uninstall -k --user 0 com.samsung.android.app.talkback
#screensaver stuff
adb shell pm uninstall -k --user 0 com.android.dreams.basic
adb shell pm uninstall -k --user 0 com.android.dreams.phototable
#game tools
adb shell pm uninstall -k --user 0 com.samsung.android.game.gametools
#simple sharing
adb shell pm uninstall -k --user 0 com.samsung.android.app.simplesharing
#another theme center
adb shell pm uninstall -k --user 0 com.samsung.android.themecenter
#samsung internet browser
adb shell pm uninstall -k --user 0 com.sec.android.app.sbrowser
#health apps
adb shell pm uninstall -k --user 0 com.sec.android.service.health
adb shell pm uninstall -k --user 0 com.sec.android.app.shealth
#samsung scloud
adb shell pm uninstall -k --user 0 com.samsung.android.scloud
#for pairing watches again
adb shell pm uninstall -k --user 0 com.samsung.android.app.watchmanagerstub
#game optimizing
adb shell pm uninstall -k --user 0 com.enhance.gameservice
#google hangouts
adb shell pm uninstall -k --user 0 com.google.android.talk
#handwriting
adb shell pm uninstall -k --user 0 com.samsung.android.sdk.handwriting
#samsung video
adb shell pm uninstall -k --user 0 com.samsung.android.video
